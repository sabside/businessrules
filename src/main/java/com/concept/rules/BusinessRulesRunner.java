package com.concept.rules;

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;

public class BusinessRulesRunner {

	/**
	 * @param mustThrow indicates that the rule expects a ServiceException if failed or not.
	 */
	public static RuleState run(final Facts facts, final Rule rule, final boolean mustThrow) throws Exception {

		final RuleState state = new RuleState();
		final RulesEngine rulesEngine = new DefaultRulesEngine();
		facts.put("state", state);

		// execute
		final Rules rules = new Rules();
        rules.register(rule);
        rulesEngine.fire(rules, facts);
		
		if (mustThrow)
			state.validate();
      	
      	return state;
	}

	public static RuleState run(final Facts facts, final Rule rule) throws Exception {

		final RuleState state = new RuleState();
		final RulesEngine rulesEngine = new DefaultRulesEngine();
		facts.put("state", state);

		// execute
		final Rules rules = new Rules();
        rules.register(rule);
        rulesEngine.fire(rules, facts);
      	state.validate();
      	
      	return state;
	}
}
