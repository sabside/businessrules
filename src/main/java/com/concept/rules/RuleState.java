/**
 * 
 */
package com.concept.rules;

import javax.validation.ValidationException;

/**
 * @author f3557790 <sabelo.simelane@fnb.co.za>
 */
public class RuleState {

	private String message = "";
	private Object result;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isPassed(){
		return this.message.isEmpty();
	}
	
	public void validate() throws ValidationException {
		if (!this.isPassed()) throw new ValidationException(this.getMessage());
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getResult(){
		return (T)result;
	}
	
	public void setResult(Object result) {
		this.result = result;
	}
}
